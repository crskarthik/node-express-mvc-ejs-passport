/**
 * @app.spec.js - tests the server app and default responses
 *
 *
 */

process.env.NODE_ENV = 'test'
const app = require('../app.js')
const mocha = require('mocha')
const chai = require('chai')
const request = require('supertest')
const expect = chai.expect
const LOG = require('../utils/logger.js')

LOG.info('Starting app.spec.js.')

// Chai assert and expect do not modify Object.prototype
// Chai should does modify Object.prototype

// Chai assert for TDD
// Chai expect and should for BDD

mocha.describe('API Test Suite', function () {
  mocha.describe('GET /', function () {
    mocha.it('responds with status 200', function (done) {
      request(app)
        .get('/')
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(200)
          done()
        })
    })
  })
  mocha.describe('GET /xyz', function () {
    mocha.it('responds with status 404', function (done) {
      request(app)
        .get('/xyz')
        .end(function (err, res) {
          if (err) { }
          expect(res.status).to.be.equal(404)
          done()
        })
    })
  })
})
